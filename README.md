## Portofolio :smile:
Bienvenue dans mon portfolio ! Ce dépôt contient le code HTML et CSS de mon site web personnel. Ci-dessous, vous trouverez un bref aperçu de la structure et du contenu du portfolio.

### Contenu 
- index.html: Le fichier HTML principal pour le portfolio.
- portofolio2.html: Un fichier HTML supplémentaire présentant mes projets de portfolio.
- Dossier Asset: Contient des sous-dossiers pour CSS et Images utilisés dans le portfolio.
- Dossier CSS: Contient les fichiers de feuilles de style.
- Dossier Images: Contient les images utilisées dans le portfolio.

[Visitez Mon Portfolio](https://portfolio-rejdaqati-4230d90a83062b39d681b9ea534f544a04f339d8ff0.gitlab.io/)

### Projets du Portfolio
-  [Courstache](https://courstache-rejdaqati-ce7015cbbbece5f32746b35895d41ae04deef18b38.gitlab.io/)
-  [Flexbox](https://flexbox-rejdaqati-2e84b0ab159cca862115fe26e0ef867760cecbec0717b.gitlab.io/)
-  [Positions](https://positions-rejdaqati-7f3769a8631f239d4e4a403eda140a444a5bb30be6b.gitlab.io/)
-  [La Pomme](https://la-pomme-rejdaqati-5c5717e0c4c369a68446c68280d986cd5f7efd97e399.gitlab.io/)
-  [Tableaux](https://tableau-rejdaqati-91cf8f546a281c76eb1067364dc5078805de3296bc97b.gitlab.io/)
-  [HTML CV](https://cv-rejdaqati-a1bc18a559323555c211a88c7ef39cb0a75fa79c4717f286ca.gitlab.io/)

## Auteurs
- [Rejda Qati](https://gitlab.com/RejdaQati)
